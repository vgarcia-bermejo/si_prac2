<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Pagina de compra de Películas">
  <meta name="Keywords" content="HTML, CSS">
  <meta name="Authors" content="Javier Encinas y Víctor García-Bermejo">
  <meta http-equiv="refresh" content="30">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Fuente escogida para la página -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <!-- Nombre de la página -->
  <title> Peliculas La Foca </title>
</head>

<body>
  <!-- Primer menu -->

  <div class="menu">
    <div class="logo">
      <a href="record.php"><img class="logo" src="media/seal.png" alt="Logo foca"></a>
    </div>
    <div class ="nombre">
      <a href="index.php"><img class="nombre" src="media/foca2.png" alt="LA FOCA"></a>
    </div>


      <?php
        session_start();
        $usuario = $_SESSION["usuario"];
        if(isset($usuario)){
          print('<div class="login">
            <a href="./record.php">'.$usuario.'</a>
          </div>
          <div class="registro">
            <a href="./destroysession.php">Logout</a>
          </div>');
        }else{
          print('<div class="login">
            <a href="./login.php">Login</a>
          </div>
          <div class="registro">
            <a href="./register.html">Registro</a>
            </div>');
        }
        ?>

  </div>

  <!-- Segundo Menu -->

  <div class="menu2">
    <div class="carrito">
      <a href="carro.php"><img id="carrito" alt="Carrito" src="media/carrito.png"></a>
    </div>
    <div class="buscador">
      <form method="get" action="busquedaPelicula.php"><input type="text" placeholder="SEARCH" name="q" ></form>
      <div class="lupa"><img src="media/lupa.png" alt="lupa" id="lupa"></div>
    </div>
</div>

    <!-- Estructura -->
    <div class ="estructura">
      <div class="col-2 lateral">
        <ul class="desplegable">
          <li><a href="">Noticias</a></li>
          <li><a href="">Estrenos</a></li>
          <li><a href="">Géneros</a>
          <ul class="generos">
            <li><a href="busquedaGenero.php?val=Accion" > Acción</a><br></li>
            <li><a href="busquedaGenero.php?val=Drama" > Drama</a><br></li>
            <li><a href="busquedaGenero.php?val=Comedia" > Comedia</a><br></li>
            <li><a href="busquedaGenero.php?val=CienciaFiccion" > Ciencia Ficción</a><br></li>
            <li><a href="busquedaGenero.php?val=Fantasia" > Fantasía</a><br></li>
            <li><a href="busquedaGenero.php?val=Terror" > Terror</a><br></li>
            <li><a href="busquedaGenero.php?val=Romance" > Romance</a><br></li>
            <li><a href="busquedaGenero.php?val=Musical" > Musical</a><br></li>
            <li><a href="busquedaGenero.php?val=Suspense" > Suspense</a><br></li>
          </ul>
          </li>
        </ul>
      </div>
      <form action="./film.php" method ="post">
      <?php
        session_start();
        $xmlUrl = 'catalogo.xml';
        $xmlSlr = file_get_contents($xmlUrl);
        $xmlStr = stripslashes($xmlSlr);
        $xml = simplexml_load_string($xmlStr);

        $titlee = $_GET["q"];
        $contador = 0;
        $contadorpelis = 0;
        /*$valor = 0;*/
        print('<div class="col-9 contenido">');

        foreach ($xml->children() as $pelicula) {

          if($pelicula->titulo == $titlee){

            if($contador == 0){
              print('<div class="fila">');
            }
            print('<div class="col-4" >
                  <div class="car">
                    <a href="film.php?val='.$pelicula->titulo.'"><img class="'.$pelicula->titulo.'" src="'.$pelicula->poster.'" alt="'.$pelicula->titulo.'"></a>
                  </div>
                </div>
                ');

                if($contador == 2){
                  print("</div>");
                  $contador =-1;
                }
                $contador = $contador +1;
                $contadorpelis = $contadorpelis +1;
              break;
        }
      }
      if($contadorpelis == 0){
          print('<h2> No se encontraron peliculas con este titulo. Por favor pruebe con otro. </h2>');

      }

      if($contador != 2){
          print('</div>');
      }




      if($contadorpelis <= 2){
          print('<div class ="fila">
          </div>');
          print('<div class ="fila">
          </div>');
          print('<div class ="fila">
          </div>');
          print('<div class ="fila">
          </div>');
      }
      if($contadorpelis <= 5){
          print('<div class ="fila">
          </div>');
          print('<div class ="fila">
          </div>');
          print('<div class ="fila">
          </div>');
          print('<div class ="fila">
          </div>');
      }
      print('</div>');
       ?>
     </form>
    <!-- footer -->
  <div class="filafooter">
    <p class="copyright"><br><br>© Copyright by Javier Encinas y Víctor García-Bermejo. All Rights Reserved.</p>
  </div>
</body>
</html>
