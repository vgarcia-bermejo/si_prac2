<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Pagina de compra de Películas">
  <meta name="Keywords" content="HTML, CSS">
  <meta name="Authors" content="Javier Encinas y Víctor García-Bermejo">
  <meta http-equiv="refresh" content="30">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <!-- Fuente escogida para la página -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Ropa+Sans" rel="stylesheet">

  <!-- Nombre de la página -->
  <title> Peliculas La Foca </title>
</head>

<body>
  <!-- Primer menu -->


    <div class="menu">
      <div class="logo">
        <a href="record.php"><img class="logo" src="media/seal.png" alt="Logo foca"></a>
      </div>
      <div class ="nombre">
        <a href="index.php"><img class="nombre" src="media/foca2.png" alt="LA FOCA"></a>
      </div>


        <?php
          session_start();
          $usuario = $_SESSION["usuario"];
          if(isset($usuario)){
            print('<div class="login">
              <a href="./record.php">'.$usuario.'</a>
            </div>
            <div class="registro">
              <a href="./destroysession.php">Logout</a>
            </div>');
          }else{
            print('<div class="login">
              <a href="./login.php">Login</a>
            </div>
            <div class="registro">
              <a href="./register.html">Registro</a>
              </div>');
          }
          ?>

    </div>

    <!-- Segundo Menu -->

    <div class="menu2">
      <div class="carrito">
        <a href="carro.php"><img id="carrito" alt="Carrito" src="media/carrito.png"></a>
      </div>
      <div class="buscador">
        <form method="get" action="busquedaPelicula.php"><input type="text" placeholder="SEARCH" name="q" ></form>
        <div class="lupa"><img src="media/lupa.png" alt="lupa" id="lupa"></div>
      </div>
  </div>
    <!-- Estructura -->
  <div class ="estructura">
    <div class="col-2 lateral">
      <ul class="desplegable">
        <li><a href="">Noticias</a></li>
        <li><a href="">Estrenos</a></li>
        <li><a href="">Géneros</a>
        <ul class="generos">
          <li><a href="busquedaGenero.php?val=Accion" > Acción</a><br></li>
          <li><a href="busquedaGenero.php?val=Drama" > Drama</a><br></li>
          <li><a href="busquedaGenero.php?val=Comedia" > Comedia</a><br></li>
          <li><a href="busquedaGenero.php?val=CienciaFiccion" > Ciencia Ficción</a><br></li>
          <li><a href="busquedaGenero.php?val=Fantasia" > Fantasía</a><br></li>
          <li><a href="busquedaGenero.php?val=Terror" > Terror</a><br></li>
          <li><a href="busquedaGenero.php?val=Romance" > Romance</a><br></li>
          <li><a href="busquedaGenero.php?val=Musical" > Musical</a><br></li>
          <li><a href="busquedaGenero.php?val=Suspense" > Suspense</a><br></li>
        </ul>
        </li>
      </ul>
    </div>


    <?php
      session_start();
      $xmlUrl = 'catalogo.xml';
      $xmlSlr = file_get_contents($xmlUrl);
      $xmlStr = stripslashes($xmlSlr);
      $xml = simplexml_load_string($xmlStr);
      $title = $_GET["val"];

      if(isset($_GET["val"])){
        foreach ($xml->children() as $pelicula) {
            if($title == $pelicula->titulo){
              $_SESSION['peliaux'] = $title;
              ?>
              <form action = "./sesionaddcarrito.php" method ="post">
              <div class="col-10 contenido">
                      <div class="fila fichatitulo">
                        <br><h1><?php echo $pelicula->titulo ?></h1>
                      </div>
                      <div class="fila informacion">
                        <div class="col-3">
                          <img class="foto" alt="Portada pelicula" src="<?php echo $pelicula->poster ?>">
                        </div>
                        <div class="col-1 datos">
                          <ul class="datoslista">
                            <li>Año:</li>
                            <li><br>Director:</li>
                            <li><br>Reparto: </li>
                            <li><br><br><br>Géneros: </li>
                          </ul>
                        </div>
                        <div class="col-7 datoscompletos">
                          <ul class="datoscompletoslista">
                            <li><?php echo $pelicula->anno;?></li>
                            <li><br><?php echo $pelicula->director;?></li>
                          <li><br><?php $actores = $pelicula->actores;
                          foreach($actores->children() as $actor){
                              print($actor->nombre.', ') ;
                          }?></li>
                            <li><br><br><?php $categorias = $pelicula->categorias;
                            foreach($categorias->children() as $categoria){
                                print($categoria.', ') ;
                            }?></li>
                          </ul>
                        </div>
                      </div>





                    <div class="col-1">
                      <ul class="sinopsisnombre">
                        <li><br>Sinopsis:</li>
                      </ul>
                    </div>
                    <div class="col-10 descsinopsis">
                      <ul>
                        <li><br><?php echo $pelicula->sinopsis ?></li>
                      </ul>
                    </div>

                    <div class="fila botoncompra">
                      <input class="button" type="submit" value="Añadir al carro" name=Anyadir>
                    </div>

                  </div>
                </form>
              </div>
                <?php
              break;
        }
      }
    }
     ?>


  <div class="filafooter">
    <p class="copyright"><br><br>© Copyright by Javier Encinas y Víctor García-Bermejo. All Rights Reserved.</p>
  </div>
</body>
</html>
